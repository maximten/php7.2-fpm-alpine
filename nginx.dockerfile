FROM nginx:alpine
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
COPY src /var/www
EXPOSE 80 443